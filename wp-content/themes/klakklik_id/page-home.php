<?php
/**
 * Template Name: Home
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 * 
 * @package klakklik_id
 * @since Feb 2015
 */

get_header(); ?>

	<!-- First View -->
	<div id="hero" class="static-header register-version light clearfix">

        <div class="text-heading">
            <h1 class="animated hiding" data-animation="bounceInDown" data-delay="0">Do not wait &mdash; <span class="highlight">launch</span> your startup now!</h1>
            <p class="animated hiding" data-animation="fadeInDown" data-delay="500">this tempate is flexible enough to suit any kind of startup or new business</p>
        </div>
		
		<div class="container">
			<div class="signup-wrapper animated hiding" data-animation="bounceInUp" data-delay="0">
				<div class="row">
						<form class="form-inline form-register form-register-small" method="post" action="signup.php">
							<div class="form-group">
								<input size="30" type="text" class="form-control required" name="fullname" id="fullname" placeholder="Your name">
							</div>							
							<div class="form-group">
								<input size="25" type="email" class="form-control required email" name="email" id="email" placeholder="Email">
							</div>
							<div class="form-group password-wrapper">
								<input type="password" class="form-control required" name="password" id="password" placeholder="Password">
							</div><br>
							<div class="form-group">
								<input size="60" type="text" class="form-control required" name="description" id="description" placeholder="Tell me what do you need from us">
							</div>
							<div class="form-group submit-wrap">
								<input type="hidden" name="small-form"/>
								<button type="submit" class="btn btn-primary btn-md">SEND NOW!</button>
							</div>							
						</form>
				</div>
			</div>
		</div>
		
    </div>

    <!-- About -->
    	<section id="about" class="section dark">
        <div class="container">

                <div class="tab-pane" id="third-tab-alt">
					<div class="section-header animated hiding center" data-animation="fadeInDown">
						<h2>3 EASY STEPS</h2>
						<div class="sub-heading">
							Lorem ipsum dolor sit atmet sit dolor greand fdanrh s
							<br />dfs sit atmet sit dolor greand fdanrh sdfs
						</div>
					</div>
					<div class="section-content row">
						<div class="col-sm-4">
							<article class="text-center animated hiding" data-animation="fadeInLeft" data-delay="0">
								<i class="howitworks icon icon-shopping-04 icon-active"></i>
								<span class="h7">PLACE ORDER</span>
								<p class="thin" >Sit amet, consectetur adipiscing elit.<br />In hac divisione rem ipsam prorsus probo<br />elegantiam desidero.</p>
							</article>
							<!--<span class="icon icon-arrows-04"></span>-->
						</div>
						<div class="col-sm-4">
							<article class="text-center animated hiding" data-animation="fadeInLeft" data-delay="400">
								<i class="howitworks icon icon-seo-icons-03 icon-active"></i>
								<span class="h7">OUR SYSTEM RUNS</span>
								<p class="thin" >Sit amet, consectetur adipiscing elit.<br />In hac divisione rem ipsam prorsus probo<br />elegantiam desidero.</p>
							</article>
							<!--<span class="icon icon-arrows-04"></span>-->
						</div>
						<div class="col-sm-4">
							<article class="text-center animated hiding" data-animation="fadeInLeft" data-delay="800">
								<i class="howitworks icon icon-seo-icons-05 icon-active"></i>
								<span class="h7">RECEIVE REPORT</span>
								<p class="thin" >Sit amet, consectetur adipiscing elit.<br />In hac divisione rem ipsam prorsus probo<br />elegantiam desidero.</p>
							</article>
						</div>
					</div>
					<br/>
					<br/>
				</div>	
			</div>
        </div>
    </section>

    <!-- Services -->
        <section id="services" class="section inverted">
        <div class="container">
            <div class="section-content">
                <div id="featuredTab">
                    <ul class="list-unstyled animated hiding" data-animation="fadeInRight">
                      <li class="active">
                          <a href="#home" data-toggle="tab">
                              <div class="tab-info">
                                  <div class="tab-title">Lorem Dolor</div>
                                  <div class="tab-desc">Sit amet, consectetur adipiscing elit<br />hac divisione rem ipsam prorsus</div>
                              </div>
                              <!--<div class="tab-icon"><span class="icon icon-multimedia-20"></span></div>-->
                          </a>
                      </li>
                      <li>
                          <a href="#profile" data-toggle="tab">
                              <div class="tab-info">
                                  <div class="tab-title">Lorem Dolor</div>
                                  <div class="tab-desc">Sit amet, consectetur adipiscing elit<br />hac divisione rem ipsam prorsus</div>
                              </div>
                              <!--<div class="tab-icon"><span class="icon icon-seo-icons-27"></span></div>-->
                          </a>
                      </li>
                      <li>
                          <a href="#messages" data-toggle="tab">
                              <div class="tab-info">
                                  <div class="tab-title">Lorem Dolor</div>
                                  <div class="tab-desc">Sit amet, consectetur adipiscing elit<br />hac divisione rem ipsam prorsus</div>
                              </div>
                              <!-- <div class="tab-icon"><span class="icon icon-seo-icons-28"></span></div>-->
                          </a>
                      </li>
                    </ul>
                    
                    <div class="tab-content animated hiding" data-animation="fadeInLeft">
                      <div class="tab-pane in active" id="home"><img src="<?php bloginfo('template_directory'); ?>/assets/img/features/rich_features_1.png" class="img-responsive animated hiding" data-animation="fadeInLeft" alt="macbook" /></div>
                      <div class="tab-pane" id="profile"><img src="<?php bloginfo('template_directory'); ?>/assets/img/features/rich_features_2.png" class="img-responsive animated hiding" data-animation="fadeInLeft" alt="macbook" /></div>
                      <div class="tab-pane" id="messages"><img src="<?php bloginfo('template_directory'); ?>/assets/img/features/rich_features_3.png" class="img-responsive animated hiding" data-animation="fadeInLeft" alt="macbook" /></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Team -->
    	<section id="team" class="section dark">
        <div class="container">
            <div class="section-header animated hiding" data-animation="fadeInDown">
                <h2>BEHIND <span class="highlight">THE</span> SCENES</h2>
				<div class="sub-heading">
                    Lorem ipsum dolor sit atmet sit dolor greand fdanrh s
                    <br>dfs sit atmet sit dolor greand fdanrh sdfs
                </div>
				<br/>
				<p>In his igitur partibus duabus nihil erat, quod Zeno commuta rest gestiret. Sed virtutem ipsam inchoavit, nihil<br/> ampliusuma. Scien tiam pollicentur, quam non erat mirum sapientiae lorem cupido patria esse cariorem. Quae<br/> qui non vident, nihilamane umquam magnum ac cognitione. In his rest gestiret. Sed ipsam inchoavit, nihil ampliusuma.<br/> Scien tiam pollicentur, quam non erat mirum sapientiae lorem cupido patria esse cariorem. Quae qui non vident,<br/> nihilamane umquam magnum ac cognitione.</p>
            </div>
			<div class="section-content row">
                        <div class="member col-md-3 col-sm-3 col-xs-6 animated hiding" data-animation="fadeInDown">
                            <div class="thumb-wrapper">
								<div class="overlay img-circle"></div>
								<div class="socials">
									<a href="#"><span class="icon icon-socialmedia-08"></span></a><a href="#"><span class="icon icon-socialmedia-07"></span></a><a href="#"><span class="icon icon-socialmedia-05"></span></a><a href="#"><span class="icon icon-socialmedia-09"></span></a>
								</div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/people/team-1.jpg" class="img-responsive img-circle" alt="thumb" />
                            </div>
							<span class="h7 highlight">DANIEL DASH</span>
							<p class="thin">Designer</p>
                        </div>  
						
						<div class="member col-md-3 col-sm-3 col-xs-6 animated hiding" data-animation="fadeInDown" data-delay="250">
                            <div class="thumb-wrapper">
								<div class="overlay img-circle"></div>
                                <div class="socials">
									<a href="#"><span class="icon icon-socialmedia-08"></span></a><a href="#"><span class="icon icon-socialmedia-07"></span></a><a href="#"><span class="icon icon-socialmedia-05"></span></a><a href="#"><span class="icon icon-socialmedia-09"></span></a>
								</div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/people/team-4.jpg" class="img-responsive img-circle" alt="thumb" />
                            </div>
							<span class="h7 highlight">PETRA APPLES</span>
							<p class="thin">Designer</p>
                        </div>     
						
						<div class="member col-md-3 col-sm-3 col-xs-6 animated hiding" data-animation="fadeInDown" data-delay="500">
                            <div class="thumb-wrapper">
								<div class="overlay img-circle"></div>
                                <div class="socials">
									<a href="#"><span class="icon icon-socialmedia-08"></span></a><a href="#"><span class="icon icon-socialmedia-07"></span></a><a href="#"><span class="icon icon-socialmedia-05"></span></a><a href="#"><span class="icon icon-socialmedia-09"></span></a>
								</div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/people/team-3.jpg" class="img-responsive img-circle" alt="thumb" />
                            </div>
							<span class="h7 highlight">MIKE LYNTON</span>
							<p class="thin">Designer</p>
                        </div>                        
						
						<div class="member col-md-3 col-sm-3 col-xs-6 animated hiding" data-animation="fadeInDown" data-delay="750">
                            <div class="thumb-wrapper">
								<div class="overlay img-circle"></div>
                                <div class="socials">
									<a href="#"><span class="icon icon-socialmedia-08"></span></a><a href="#"><span class="icon icon-socialmedia-07"></span></a><a href="#"><span class="icon icon-socialmedia-05"></span></a><a href="#"><span class="icon icon-socialmedia-09"></span></a>
								</div>
                                <img src="<?php bloginfo('template_directory'); ?>/assets/img/people/team-2.jpg" class="img-responsive img-circle" alt="thumb" />
                            </div>
							<span class="h7 highlight">AMY PIERS</span>
							<p class="thin">Designer</p>
                        </div>

            </div>
        </div>
    </section>

    <!-- Contacts -->
        <section id="guarantee" class="long-block light">
        <div class="container">
            <div class="col-md-12 col-lg-9">
				<i class="icon icon-seo-icons-24 pull-left"></i>
                <article class="pull-left">
                    <h2><strong>LAUNCH</strong> YOUR STARTUP NOW!</h2>
                    <p class="thin">In his igitur partibus duabus nihil erat, quod Zeno commuta rest gestiret.</p>
                </article>
            </div>
			
            <div class="col-md-12 col-lg-3">
                <a href="hthttp://themeforest.net/item/startuply-responsive-multipurpose-landing-page/7953388" class="btn btn-default">Get this template</a>
            </div>
        </div>
    </section>

<?php get_footer(); ?>
