<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package klakklik_id
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta name="description" content="Klak Klik ID">
	<meta name="keywords" content="Klak Klik ID">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>

	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/assets/img/favicon.ico">
	<link rel="apple-touch-icon" href="<?php bloginfo('template_directory'); ?>/assets/img/apple-touch-icon.jpg">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php bloginfo('template_directory'); ?>/assets/img/apple-touch-icon-72x72.jpg">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php bloginfo('template_directory'); ?>/assets/img/apple-touch-icon-114x114.jpg">
	<link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/bootstrap.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/font-awesome.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/font-lineicons.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/animate.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/toastr.min.css" type="text/css" media="all" />
	<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/assets/css/style.css" type="text/css" media="all" />

    <!--[if lt IE 9]>
        <script src="<?php bloginfo('template_directory'); ?>/assets/js/html5.js"></script>
        <script src="<?php bloginfo('template_directory'); ?>/assets/js/respond.min.js"></script>
        <![endif]-->

    </head>

    <body <?php body_class(); ?> id="landing-page">
    	<!-- Preloader -->
    	<div id="mask">
    		<div id="loader"></div>
    	</div>

    	<div id="page" class="hfeed site">
    		<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'klakklik_id' ); ?></a>

    		<header>
    			<nav class="navigation navigation-header">
    				<div class="container">
    					<div class="navigation-brand">
    						<div class="brand-logo">
    							<a href="index.html" class="logo"></a>
    							<span class="sr-only">startup.ly</span>
    						</div>
    						<button class="navigation-toggle visible-xs" type="button" data-toggle="dropdown" data-target=".navigation-navbar">
    							<span class="icon-bar"></span>
    							<span class="icon-bar"></span>
    							<span class="icon-bar"></span>
    						</button>
    					</div>
    					<div class="navigation-navbar">
    						<ul class="navigation-bar navigation-bar-left">
    							<li class="active"><a href="#hero">Home</a></li>
    							<li><a href="#about">About</a></li>
    							<li><a href="#services">Services</a></li>
    							<li><a href="#team">Team</a></li>
    							<li><a href="#guarantee">Contacts</a></li>
    						</ul>
    						<ul class="navigation-bar navigation-bar-right">
    							<li><a href="register.html">Login</a></li>
    							<li class="featured"><a href="register.html">Sign up</a></li>
    						</ul>  
    					</div>
    				</div>
    			</nav>
    		</header>

    		<div id="content" class="site-content">
